public class Main {

	public static void main(String[] args) {	

        // Konstruktoren
        // =======================================

        System.out.println("new Kind()");
        Parent parentErika = new Kind(); 
            // Wo ist der Konstruktor definiert?
            // Falls kein Konstruktor in der Klasse definiert ist, wird automatisch der default Konstruktor definiert.
										  
        // Der Default Konstruktor sieht genau wie folgt aus:
        /*       
            Kind() { 
                super(); 
            }
        */
        System.out.println("new Parent()");
        Parent parentOtto = new Parent(); 
            // Was geschieht mit dem Feld name? Welchen Wert hat name?
            
            // Wenn auch nicht explizit auscodiert wird immer 
            // folgendes Statement als erstes von jedem konstruktor ausgefuehrt
        /*     
            super();      
        */
        System.out.println("new Parent(Parentwithname \"Heinz\")");
        Parent parentHeinz = new Parent("Heinz");
        
        System.out.println("new Enkel(GrandChildname \"Soraia\")");
        Parent parentSofia = new Enkel("Soraia");
        
        Enkel enkelPaul = new Enkel(); 
            // Wieso gibt es von Enkel keinen Default-Konstruktor?
            // Sie sieht ja fast gleich aus wie Kind Klasse?
            // Sobald ein Konstruktor explizit erstellt wird
            // gibt es keinen Default-Konstruktor mehr!


        // Was ist dynamische Bindung von Methoden?
        // ========================================
            // Hier wird die dump() Methode der Kind-Klasse aufgerufen, obwohl 
            // die Variable als Parent Typ deklariert wurde.
            // -> Dynamisch waehrend der Laufzeit wird eine Methode dump() in der Klasse Kind 
            // gesucht und falls gefunden, auch ausgefuehrt.
            // Der Compiler weiss ja noch nicht welcher Objekt-Typ da wirklich drin sein wird. 
            // Aber ist es nicht sehr ineffizient zu suchen? 
            // Es wird eigentlich nicht gesucht, sondern effizient 
            // ueber einen Pointer auf eine vTable der Klasse welcher das Objekt angehoert, 
            // die entsprechende Methode gefunden.
            // Dies ist Dank der starken Typisierung und Single Inheritance in Java sehr 
            // effizient zu implementieren. Und selbst das wird noch optimiert. 
            
            // Guter Eintrag im Blog, falls sich jemand für die Internals interessieren sollte. 
            // https://www.quora.com/How-exactly-does-JVM-implement-virtual-method-invocation.
            
        parentErika.dump(); 
            // Hier geschieht was allgemein erwartet wird. Es wird die Methode 
            // der Parentklasse ausgefuehrt.
            
            // Aber was ist mit der Variable 'name' die ausgegeben wird? 
            // Sie wurde ja gar nicht gesetzt, oder doch?
        parentOtto.dump();
    		// Hier geschieht was allgemein erwartet wird. Nichts besonderes
        
            // Welche Routine wird jetzt ausgefuehrt? 
        parentHeinz.dump();
            // Die Enkel Klasse hat keine Methode dump(), 
            // aber die Klasse Kind und die Klasse Parent enthält diese.

            // Welche der beiden wird hier ausgefuehrt.
        parentSofia.dump();
		
            // Wird die @Deprecated Annotation der Klasse Parent vererbt?
        System.out.println("new Kind()"); 
        Kind kindMarkus = new Kind();
        kindMarkus.dump(); 
            // Anscheindend nicht -> Keine Warning hier!
            // Annotations werden nicht vererbt, ausser sie 
            // wurden mit der Annotation @Inherited definiert.
    
        // Wie "Casted" man zwischen verschiedenen Objekt Klassen?
        // =======================================================
            // Hier ein Beispiel, das funktioniert, da 'parentErika' oben als  
            // ein Objekt von der Klasse Kind erzeugt wurde.
        Kind kindErna = (Kind)parentErika;
        kindErna.dumpall();
            // Diese Zeile wird zur Laufzeit eine ClassCastException erzeugen, 
            // da 'parentOtto' ein Objekt der Klasse Parent ist.
        Kind kindOdette = (Kind)parentOtto;
        kindOdette.dumpall();
        
            // Funktioniert dies?
        Kind kind1VonSofia = (Enkel)parentSofia; 
            // Ja, das parentSofia in Realitaet ein 
            // Objekt der Klasse Enkel, welche Kind als Parentklasse hat.
    
            // Wieso kann dies nicht kompiliert werden?
        Kind kind2VonSofia = parentSofia; 
            // parentSofia ist oben mit new Enkel("...") erzeugt worden. 
            // Warum geht es trotzdem nicht? 
            // parentSofia ist immer noch vom Typ Parent fuer den Compiler und 
            // Kind aber nicht der Parent von Parent.
            
        parentErika.dumpall();   
            // parentErika ist oben mit new Kind("...") erzeugt worden. 
            // Warum geht es trotzdem nicht? 
            // parentErika ist immer noch vom Typ Parent fuer den Compiler 
            // und in dieser Klasse gibt es kein dumpall().

        ((Kind)parentErika).dumpall(); 
            // So geht es, aber dabei ist hier immer ein Risiko für eine ClassCastException	
	}
}
