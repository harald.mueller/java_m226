/**
 * 
 * @author ado
 * Das ist die Parent Klasse mit einem Konstruktor Parent(String name)
 * und einer Deprecated methode dump()
 */
public class Parent {

	String name;
	@Deprecated
	public void dump() {
		System.out.println("Das ist dump von Parent mit name="+name);
	}
		
	Parent(String name){
		/* Hier wird vom Kompiler 
		 * super();
		 * eingefuegt
		 */
		System.out.println("Dies ist der Konstruktor Parent(name)");
		this.name=name;
	}
    /*
     * Hier fehlt der Konstruktor
     * Parent(){} 
     * damit der default Konstruktor von Kind kompiliert werden kann.		
     */
}


