
public class Enkel extends Kind {

	/**
	 * Bei dieser Klasse wird ein Konstruktor Enkel(String name) definiert 
	 * und damit existiert der default Konstruktor  Enkel() nicht mehr.
	 * Diese Klasse erbt von Kind die Methoden dump() und dumpall()
	 * 
	 * @param name
	 */
	Enkel(String name) {
		/* Hier wird vom Compiler
		 * super() eingefuegt
		 */
		System.out.println("Dies ist der Konstruktor des Enkel(name)");
		this.name = "Enkel Name = "+name;
	}
	
}
