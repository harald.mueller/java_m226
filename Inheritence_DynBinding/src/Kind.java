/** 
 * 
 * @author ado
 * Bei dieser klasse wird kein Konstruktor explizit definiert.
 * -> Es wird automatisch der default Konstruktor definiert. Damit dieser funktioniert muss es in
 * der Parent Klasse entweder explizit einen Konstruktor mit keine argumenten geben, also Parent()
 * oder ueberhaupt keinen Konstruktor, da dann automatisch vom Kompiler der default Konstruktor definiert wird.
 * Diese Klasse hat 2 methoden: 
 * dump() welche dump() von Parent ueberschreibt 
 * zusaetzliche dumpall()
 */
public class Kind extends Parent {
	int value;
	String stringvalue;
	
	/* Hier wird vom Kompiler 
	 * Kind(){ 
	 *    super();
	 * } 
	 * eingefuegt, da kein Konstruktor definiert ist. Diese kann nicht Kompiliert werden, 
	 * da der Konstruktur Parent() nicht existiert. Darum der Fehler bei der Zeile
	 * public class Kind extends Parent {
	 */
	@Override
	/* Diese Methode ist nicht Deprecated markiert obwohl dump() von Parent deprecated ist
	 * Die Annotation Deprecated wird also nicht vererbt!!
	 */
	public void dump() {
		System.out.println("Das ist dump von Kind und hat value=" + 
					value + " und stringvalue=" + stringvalue);
	}

	public void dumpall() {

	}
    
	
}
